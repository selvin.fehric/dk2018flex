
%{
#include "token.hpp"
%}

ws            [ \t\n]
letter        [a-zA-Z]
digit         [0-9]
number        {digit}+
identifier    {letter}({letter}|{digit})*
special	      [?\-;.!$#]

%%

{ws}
"if"                    { return IF; } 
"for"                  { return FOR; } 
"then"                    { return THEN; }
"else"                    { return ELSE; }
{identifier}                   { return ID; } 
"=="                  { return EQUAL; } 
"="                     { return ASSIGN; } 
{number}                { return NUMBER; } 
{special}		{ return SPECIAL; }
.+                     printf("nepoznat string '%s' duzine '%d' \n", yytext, (int)yyleng);


%%

int main(void){
  int tok;
  while(tok = yylex() ){
    Token t(tok, yytext);
    t.print();
  }  
  return 0;
}

int yywrap(){
  return 1;
}
