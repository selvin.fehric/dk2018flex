#ifndef _TOKEN_
#define _TOKEN_
#include <string>
#include <iostream>

const int IF = 1;
const int FOR = 2;
const int ELSE = 3;
const int THEN = 4;
const int ID = 5;
const int EQUAL = 6;
const int ASSIGN = 7;
const int NUMBER = 8;
const int SPECIAL = 9;

class Token{
  public:
    int tag;
    std::string lexeme;

    Token(int i=0, std::string const & s=""):tag(i), lexeme(s) {}
    Token& operator=(Token const & t){
      tag = t.tag;
      lexeme = t.lexeme;
      return *this;
    }
    void print(){
      std::cout << "<" << tag << ", " << lexeme << ">" << std::endl;
    }

};

#endif
